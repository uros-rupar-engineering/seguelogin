//
//  ViewController.swift
//  SegueLogin
//
//  Created by uros.rupar on 5/25/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var repeatPassword: UITextField!
    
    var ispis : String = ""
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.isEnabled = false
      
       
    }
    
    @IBAction func activateUsername(_ sender: Any) {
        if username.text!.count > 0{
            loginButton.isEnabled = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.navigationItem.title = ispis
    }
    
    @IBAction func login(_ sender: Any) {
        if validate(){
            ispis = username.text!
        }
       
    }
    
    @IBAction func forgotUser(_ sender: Any) {
        
        ispis = "Forgot username"
    }
    
    @IBAction func forgotpass(_ sender: Any) {
        
        ispis = "Forgot password"
    }
    
    func validate ()-> Bool{
        if let usernameVar = username.text, password.text!.count > 0,password.text == repeatPassword.text{
            return true
        }else if password.text!.count == 0 || repeatPassword.text!.count == 0 || username.text!.count == 0{
            makeAlert("Wrong","Some field is empty")
            return false}
        else{
                makeAlert("Wrong","repass and pass are not same")
                return false
            }
        
    }
            

    func makeAlert(_ heading:String,_ description:String){
        let alert  = UIAlertController(title: heading, message: description, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
 
}

